chrome.app.runtime.onLaunched.addListener(function() {
    chrome.app.window.create('app.html', {
        id: 'kwanggul',
        bounds: {
            width: 650,
            height: 600
        },
        resizable: false,
        singleton: true
    });
});