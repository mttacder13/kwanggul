var urlList = [
    'http://www.freshwap.me/movies',
    'http://www.freshwap.me/movies/page/2',
    'http://www.freshwap.me/movies/page/3',
    'http://www.freshwap.me/movies/page/4',
    'http://www.freshwap.me/movies/page/5'
    ];

var urls = null,
    movieTemplate = document.getElementById('movie-template').innerHTML,
    loaderTemplate = document.getElementById('loader-template').innerHTML,
    lastUpdatedElement = document.querySelector('.last-updated'),
    movieListElement = document.querySelector('.movie-list'),
    reloadElement = document.getElementById('reload'),
    movieListFragment = null,
    newlyPostedMovieItems = null,
    skippedMovieItems = null,
    dateTimeStarted, dateTimeEnded;

attachEventHandlers();
startUpdating();

function attachEventHandlers() {
    movieListElement.addEventListener('click', handleAnchorLinkClick);
    document.querySelector('.other-links').addEventListener('click', handleAnchorLinkClick);

    window.onmousewheel = document.onmousewheel = function (e) {
        if (isChildOfMovieItemElement(e.target)) {
            e.returnValue = false;
        }
    }

    reloadElement.addEventListener('click', function(e) {
        e.preventDefault();
        startUpdating();
    });

    function handleAnchorLinkClick(e) {
        if (e.target.nodeName == 'A') {
            e.preventDefault();
            window.open(e.target.href);
        }
    }
}

function startUpdating() {
    dateTimeStarted = new Date();
    newlyPostedMovieItems = [];
    skippedMovieItems = [];
    urls = urlList.slice(0);

    reloadElement.style.visibility = 'hidden';
    lastUpdatedElement.innerHTML = '<img src="res/loader-small.gif" alt="Loading...">';
    movieListElement.innerHTML = loaderTemplate;
    movieListFragment = document.createDocumentFragment();

    downloadNewlyPostedMovies(urls);
}

function endUpdating() {

    createSkippedMoviesList();
    movieListElement.innerHTML = '';
    movieListElement.appendChild(movieListFragment);
    reloadElement.style.visibility = 'visible';

    dateTimeEnded = new Date();
    lastUpdatedElement.innerHTML = dateTimeEnded.toLocaleString()
        + ' (' + (Math.round((dateTimeEnded - dateTimeStarted) / 1000)) + ' seconds)';
}

function downloadNewlyPostedMovies(urls) {
    if (urls.length) {
        ajax('get', urls.shift(), function(responseText) {
            scrapeWebContentForMovies(responseText);
            downloadNewlyPostedMovies(urls);
        });
    } else {
        downloadImdbInformation();
    }
}

function scrapeWebContentForMovies(responseText) {

    var rgx = scrapeWebContentForMovies.rgx || /http\:\/\/www\.imdb\.com\/title\/(tt\d+)/;

    var container = document.createElement('div');
    container.innerHTML = cleanHtml(responseText);

    var itemElements = container.querySelectorAll('.item');

    for (var i = 0; i < itemElements.length; i++) {
        var item = itemElements[i];
        var title = item.querySelector('.title').innerHTML.trim();
        var match = rgx.exec(item.innerHTML);
        if (match && !movieItemExists(match[1])) {
            newlyPostedMovieItems.push({
                imdbId: match[1],
                title: title
            });
        } else if (skippedMovieItems.indexOf(title) == -1) {
            skippedMovieItems.push(title);
        }
    }
}

function movieItemExists(imdbId) {
    for (var i = 0; i < newlyPostedMovieItems.length; i++) {
        if (newlyPostedMovieItems[i].imdbId == imdbId) {
            return true;
        }
    }
    return false;
}

function downloadImdbInformation() {
    if (newlyPostedMovieItems.length) {
        var movieItem = newlyPostedMovieItems.shift();

        ajax('get', 'http://www.omdbapi.com?i=' + movieItem.imdbId, function(responseText) {
            var data = null;
            if (responseText) {
                try {
                    data = JSON.parse(responseText);
                } catch (err) {
                    console.log(err.message);
                }
            }
            if (data && data.Response == 'True') {
                createNewMovieListItem(movieItem, data);
            }
            downloadImdbInformation();
        });
    } else {
        endUpdating();
    }
}

function createNewMovieListItem(movieItem, data) {
    var li = document.createElement('li');
    li.className = 'movie';
    li.innerHTML = movieTemplate;
    var titleElement = li.querySelector('.title');
    titleElement.innerHTML = movieItem.title;
    titleElement.title = titleElement.innerText;
    li.querySelector('.info .imdb').innerHTML = '<a href="http://www.imdb.com/title/'
        + data.imdbID + '">IMDb</a>';
    li.querySelector('.info .dlink').innerHTML = '<a href="http://www.300mbfilms.com/?s='
        + encodeURIComponent(data.Title + ' ' + data.Year) + '">300mbfilms</a>';
    li.querySelector('.info .actors').innerHTML = data.Actors;
    li.querySelector('.info .rating').innerHTML = data.imdbRating;
    li.querySelector('.info .rated').innerHTML = data.Rated;
    li.querySelector('.info .genres').innerHTML = data.Genre;
    li.querySelector('.info .plot').innerHTML = data.Plot;

    movieListFragment.appendChild(li);

    downloadImage(
        data.Poster.replace('_SX300', '_SX100'),
        li.querySelector('.poster img')
        );
}

function createSkippedMoviesList() {
    if (skippedMovieItems.length) {
        var ul = document.createElement('ul');
        ul.className = 'info';
        for (var i = 0; i < skippedMovieItems.length; i++) {
            var li = document.createElement('li');
            li.innerHTML = skippedMovieItems[i];
            ul.appendChild(li);
        }
        li = document.createElement('li');
        li.className = 'movie';
        li.innerHTML = '<h3 class="title">Movies with NO IMDb URL</h3>';
        li.appendChild(ul);
        movieListFragment.appendChild(li);
    }
}

function ajax(method, url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            callback(xhr.responseText);
        }
    }
    xhr.send();
}

function downloadImage(url, imgElement) {

    if (!/^http/i.test(url)) {
        imgElement.parentNode.removeChild(imgElement);
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function() {
        imgElement.src = window.webkitURL.createObjectURL(xhr.response);
        imgElement.parentNode.style.background = 'none';
    }
    xhr.open('get', url, true);
    xhr.send();
}

function cleanHtml(html) {
    var rgx = cleanHtml.rgxScript || /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (rgx.test(html)) {
        html = html.replace(rgx, '');
    }

    rgx = cleanHtml.rgxImage || /<img\b[^>]+>/gi;
    while (rgx.test(html)) {
        html = html.replace(rgx, '');
    }

    return html;
}

function isChildOfMovieItemElement(node) {
    if (node.parentNode) {
        if (node.parentNode.className == 'movie') {
            return true;
        } else {
            return isChildOfMovieItemElement(node.parentNode);
        }
    }
    return false;
}
